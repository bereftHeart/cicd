<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="overflow-hidden h-screen">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
        content="Explore a vast collection of captivating literature, soulful music, and cinematic masterpieces at our one-stop e-commerce destination. From the latest bestsellers to timeless classics, our carefully curated selection of books, CDs, and DVDs caters to every taste and interest. Immerse yourself in the worlds of words, melodies, and visual storytelling with just a few clicks." />
    <meta name="keywords" content="react,meta,tags" />
    <meta property="og:title" content="Daicy Store" />
    <meta property="og:description"
        content="Discover a world of literary wonder in our expertly curated collection of Books, CDs and DVDs" />
    <meta property="og:image" content="https://i.pinimg.com/736x/a3/c1/e8/a3c1e852bb7653982a00a353871b6bdc.jpg" />

    <title inertia>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

    <!-- Scripts -->
    @routes
    @viteReactRefresh
    @vite(['resources/js/app.jsx', "resources/js/Pages/{$page['component']}.jsx"])
    @inertiaHead
</head>

<body class="font-sans antialiased h-screen">
    @inertia
</body>

</html>
