<x-mail::message>

    Hi {{ $user->name }},

    @if ($user->role === 'manager')
        You are now an manager. You can make changed on products, approve orders.
    @else
        Your role was changed into customer.
    @endif

    Thank you for using our application.
    {{ config('app.name') }}

</x-mail::message>
