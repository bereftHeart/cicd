<x-mail::message>

    Hi {{ $order->user->name }},

    We have received your order and we are processing it.
    The order details are as follows:

    Order ID: {{ $order->id }}
    Receiver's Name: {{ $order->delivery->name }}
    Receiver's Phone: {{ $order->delivery->phone }}
    Address: {{ $order->delivery->address }}
    Order Date: {{ $order->created_at->format('d-m-Y') }}
    Order Total: {{ $order->total_price }}
    Shipping fee: {{ $order->shipping_fee }}
    Discount: {{ $order->free_ship_discount }}

    You can track your order by clicking on the button below:
    <x-mail::button url="{{ route('order.show', $order) }}" color="primary">View order</x-mail::button>

    You can also cancel your order by clicking on the button below:
    <x-mail::button url="{{ route('order.cancel', $order) }}" color="error">Cancel order</x-mail::button>

    If you have any questions about your order, please contact us at {{ config('mail.from.address') }}.
    Thank you for using our application.
    {{ config('app.name') }}

</x-mail::message>
